﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.BL.Services;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Newtonsoft.Json;



namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ParkingController : ControllerBase
    {
        IParkingService parkingservice;

        //;
        public ParkingController(IParkingService parking)
        {
            //ps = new ParkingService(new TimerService(), new TimerService(), new LogService());
            parkingservice = parking;
        }


        [HttpGet("balance")]
        public IActionResult getBalance()
        {
            // return new JsonConvert(ps.GetBalance());
            try
            {
                return new JsonResult(parkingservice.GetBalance());
            }
            catch
            {
                return BadRequest("Something wrong(");
            }
        }

        [HttpGet("capacity")]
        public IActionResult getCapacity()
        {
            // return new JsonConvert(ps.GetBalance());
            try
            {
                return new JsonResult(Settings.capacity);
            }
            catch
            {
                return BadRequest("Something wrong(");
            }
        }

        [HttpGet("freePlaces")]
        public IActionResult getFreePlaces()
        {
            // return new JsonConvert(ps.GetBalance());
            try
            {
                return new JsonResult(parkingservice.GetFreePlaces());
            }
            catch
            {
                return BadRequest("Something wrong(");
            }
        }

    }
}
