﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.BL.Models;
using CoolParking.WebAPI.Models;

namespace CoolParking.WebAPI.Services
{
    public class ConverteService
    {
        public static Vehicle ConvertModelToVehicle(VehicleModel model)
        {
            return new Vehicle(model.Id, (VehicleType)model.VehicleType, model.Balance);
        }

        public static VehicleModel ConvertVehicleToModel(Vehicle vehicle)
        {
            VehicleModel vm = new VehicleModel();
            vm.Id = vehicle.Id;
            vm.VehicleType = (int)vehicle.VehicleType;
            vm.Balance = vehicle.Balance;
            return vm;
        }

        public static TransactionInfoMoodel[] ConvertTransArrInfoToModelArr(TransactionInfo[] TranI)
        {
            TransactionInfoMoodel[] rez = new TransactionInfoMoodel[TranI.Length];
            for (int i = 0; i < TranI.Length; i++)
            {
                rez[i] = (ConvertTransInfToModel(TranI[i]));
            }
            return rez;
        } 

        public static TransactionInfoMoodel ConvertTransInfToModel(TransactionInfo ti)
        {
            TransactionInfoMoodel tim = new TransactionInfoMoodel();
            tim.vehicleId = ti.Id;
            tim.sum = ti.Sum;
            tim.transactionDate = ti.time;
            return tim;
        }
        
        public static string ConvertLogsToString(string readed)
        {
            string rez = "";
            string[] splited = readed.Split("\n");
            for(int i = 0;i < splited.Length; i++)
            {
                if(splited[i] == "\r")
                {
                    continue;
                }
                rez += splited[i].Replace("\r","");
                if (i != splited.Length - 1) rez += "\n";
            }
            return rez;
        }

        public static decimal GetProfitFromLastTransaction(TransactionInfo[] ti)
        {
                decimal sum = 0;
                for (int i = 0; i < ti.Length; i++)
                {
                    sum += ti[i].Sum;
                }
                return sum;       
        }
    }
}
