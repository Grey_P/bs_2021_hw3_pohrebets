﻿using CoolParking.BL.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.WebAPI.Services;
using CoolParking.WebAPI.Models;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TransactionsController : ControllerBase
    {
        IParkingService parkingservice;
        public TransactionsController(IParkingService parking)
        {
            parkingservice = parking;
        }

        [HttpGet("last")]
        public IActionResult GetLastTransact()
        {
            try
            {
                // What id array of last transaction empty? 
                // mby Bad Request?
                return new JsonResult(ConverteService.ConvertTransArrInfoToModelArr(parkingservice.GetLastParkingTransactions()));
            }
            catch
            {
                return BadRequest("There isn`t any transaction for this period");
            }
        }

        [HttpGet("all")]
        public IActionResult GetAllTransact()
        {
            try
            {
                return new JsonResult(ConverteService.ConvertLogsToString(parkingservice.ReadFromLog()));
            }
            catch
            {
                return NotFound("There wasn`t any transactions");
            }
        }

        [HttpPut("topUpVehicle")]
        public IActionResult TopUpVehicle([FromBody] TopUpModel tuModel)
        {
            try
            {
                if (parkingservice.GetVehicles().Count == 0)
                {
                    return NotFound("Vehicle isn`t found");
                }
                if (!parkingservice.GetVehicles()[0].isValid(tuModel.id))
                {
                    return BadRequest("Invalid input data");
                }
                for (int i = 0; i < parkingservice.GetVehicles().Count; i++)
                {
                    if (parkingservice.GetVehicles()[i].Id == tuModel.id)
                    {
                        parkingservice.TopUpVehicle(tuModel.id, tuModel.sum);
                        return new JsonResult(ConverteService.ConvertVehicleToModel(parkingservice.GetVehicles()[i]));
                    }
                }
                return NotFound("Vehicle isn`t found");
            }
            catch
            {
                return NotFound("Vehicle isn`t found or invalid input data");
            }
        }

        [HttpGet("profit")]
        public IActionResult GetProfit()
        {
            try
            {
                return new JsonResult(ConverteService.GetProfitFromLastTransaction(parkingservice.GetLastParkingTransactions()));
            }
            catch
            {
                return BadRequest("There isn`t any transaction for this period");
            }
        }
    }
}
