﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Models
{
    public class TopUpModel
    {
        [JsonProperty("id")]
        public string id { get; set; }

        [JsonProperty("Sum")]
        public decimal sum { get; set; }
    }
}
