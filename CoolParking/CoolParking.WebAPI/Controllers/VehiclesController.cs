﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Services;
using CoolParking.BL.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.WebAPI.Models;
using CoolParking.WebAPI.Services;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class VehiclesController : ControllerBase
    {
        private IParkingService parkingservice;

        public VehiclesController(IParkingService parking)
        {
            //ps = new ParkingService(new TimerService(), new TimerService(), new LogService());
            parkingservice = parking;
        }

        [HttpGet]
        public IActionResult GetVehicles()
        {
            try
            {
                List<VehicleModel> list = new List<VehicleModel>();
                for (int i = 0; i < parkingservice.GetVehicles().Count; i++)
                {
                    list.Add(ConverteService.ConvertVehicleToModel(parkingservice.GetVehicles()[i]));
                }
                return new JsonResult(list);
            }
            catch
            {
                return BadRequest();
            }
        }

        [HttpGet("{id}")]
        public IActionResult GetVehiclesByID(string id)
        {
            try
            {
                if (parkingservice.GetVehicles().Count == 0)
                {
                    return NotFound("There isn`t vehicle with this ID");
                }
                if (!parkingservice.GetVehicles()[0].isValid(id))
                {
                    return BadRequest("invalid ID");
                }
                for (int i = 0; i < parkingservice.GetVehicles().Count; i++)
                {
                    if (parkingservice.GetVehicles()[i].Id == id)
                    {
                        return new JsonResult(ConverteService.ConvertVehicleToModel(parkingservice.GetVehicles()[i]));
                    }
                }
                return NotFound("There isn`t vehicle with this ID");
            } catch
            {
                return BadRequest("invalid ID");
            }
        }

        [HttpPost]
        public IActionResult AddVehicle([FromBody] VehicleModel vm)
        {
            try
            {
                Vehicle vehicle = ConverteService.ConvertModelToVehicle(vm);
                parkingservice.AddVehicle(vehicle);
                return CreatedAtAction(nameof(AddVehicle), new { id = vm.Id }, vm);
            }
            catch
            {
                return BadRequest("invalid input format");
            }
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteVehicle(string id)
        {
            try
            {
                if(parkingservice.GetVehicles().Count == 0)
                {
                    return NotFound("There isn`t vehicle with this ID");
                    
                }
                if (!parkingservice.GetVehicles()[0].isValid(id))
                {
                    return BadRequest("Invalid input data");
                }
                parkingservice.RemoveVehicle(id);
                return NoContent();
            }
            catch (ArgumentException)
            {
                return NotFound("There isn`t vehicle with this ID");
            } 
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
