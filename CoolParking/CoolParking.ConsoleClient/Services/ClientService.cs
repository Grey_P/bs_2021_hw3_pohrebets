﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using CoolParking.ConsoleClient.Models;
using Newtonsoft.Json;

namespace CoolParking.ConsoleClient.Services
{
    public class ClientService
    {
        private string PATH;
        public ClientService(string path)
        {
            this.PATH = path;
        }

        public void AddTransportToParkingAsync()
        {
            try
            {
                VehicleModel vehicleModel = new VehicleModel();

                // id
                string id;
                Console.WriteLine("Введiть id транспорту(в форматi AB-1234-CD ):");
                Console.Write("--> ");
                id = Console.ReadLine();

                // VenicleType
                Console.WriteLine("Оберiть тип транспорту(number):");
                Console.WriteLine("1.PassengerCar");
                Console.WriteLine("2.Truck");
                Console.WriteLine("3.Bus");
                Console.WriteLine("4.Motorcycle");
                Console.Write("--> ");
                int numberVehicleType = int.Parse(Console.ReadLine());
                if (numberVehicleType < 1 || numberVehicleType > 4)
                {
                    Console.WriteLine("I don`t know what is type of your transport(");
                    Console.ReadKey();
                    Console.Clear();
                    return;
                }
                // Balance
                Console.WriteLine("Введiть початковий баланс:");
                Console.Write("--> ");
                decimal startBal = decimal.Parse(Console.ReadLine());
                vehicleModel.Id = id;
                vehicleModel.VehicleType = numberVehicleType;
                vehicleModel.Balance = startBal;

                //--------- SENDING REQUEST
                var json = JsonConvert.SerializeObject(vehicleModel);
                var data = new StringContent(json, Encoding.UTF8, "application/json");
                var url = PATH + "/vehicles";

                using (var client = new HttpClient(BypassCertificate()))
                {
                    var response = Task.Run(async () => await client.PostAsync(url, data)).Result;
                    Console.WriteLine("Status:" + response.StatusCode);
                    Console.WriteLine("Result:" + response.Content.ReadAsStringAsync().Result);
                    Console.ReadKey();
                }
            }
            catch (Exception e)
            {
                Console.Clear();
                Console.WriteLine(e.GetType());
                Console.WriteLine(e.Message);
                Console.ReadKey();
            }
        }

        public void RemoveTransportFromParking()
        {
            try
            {
                Console.WriteLine("Введiть id транспорту який забирають з паркiнгу:");
                Console.Write("--> ");
                string id = Console.ReadLine();
                var url = PATH + "/vehicles/" + id;
                using (var client = new HttpClient(BypassCertificate()))
                {
                    var response = Task.Run(async () => await client.DeleteAsync(url)).Result;
                    Console.WriteLine("Status:" + response.StatusCode);
                    if(response.StatusCode == System.Net.HttpStatusCode.NoContent)
                    {
                        Console.WriteLine("Done! Vehicle is removed.");
                    }
                    Console.ReadKey();
                }
            }
            catch (Exception e)
            {
                Console.Clear();
                Console.WriteLine(e.GetType());
                Console.WriteLine(e.Message);
                Console.ReadKey();
            }
        }

        public void UppMoneyToTransport()
        {
            try
            {
                TopUpModel topUpModel = new TopUpModel();
            Console.WriteLine("Введiть id транспорту для якого поповнюємо баланс:");
            Console.Write("--> ");
            topUpModel.Id = Console.ReadLine();
            Console.WriteLine("Введiть сумму поповнення:");
            Console.Write("--> ");
            topUpModel.Sum = decimal.Parse(Console.ReadLine());
            
            var json = JsonConvert.SerializeObject(topUpModel);
            var data = new StringContent(json, Encoding.UTF8, "application/json");
            var url = PATH + "/transactions/topUpVehicle";
                using (var client = new HttpClient(BypassCertificate()))
                {
                    var response = Task.Run(async () => await client.PutAsync(url, data)).Result;
                    Console.WriteLine("Status:" + response.StatusCode);
                    Console.WriteLine("Result:" + response.Content.ReadAsStringAsync().Result);
                    Console.ReadKey();
                }
            }
            catch (Exception e)
            {
                Console.Clear();
                Console.WriteLine(e.GetType());
                Console.WriteLine(e.Message);
                Console.ReadKey();
            }
        }

        public void OutFreePlaces()
        {
            try
            {
                var url = PATH + "/parking/freePlaces";
                using (var client = new HttpClient(BypassCertificate()))
                {
                    var response = Task.Run(async () => await client.GetAsync(url)).Result;
                    if(response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        Console.WriteLine("Result: " + response.Content.ReadAsStringAsync().Result + " places is free");
                        Console.ReadKey();
                    }
                    
                }
            }
            catch (Exception e)
            {
                Console.Clear();
                Console.WriteLine(e.GetType());
                Console.WriteLine(e.Message);
                Console.ReadKey();
            }
        }

        public void OutVehiclesOnParking()
        {
            try
            {
                var url = PATH + "/vehicles";
                using (var client = new HttpClient(BypassCertificate()))
                {
                    var response = Task.Run(async () => await client.GetAsync(url)).Result;
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        string[] vehicles = response.Content.ReadAsStringAsync().Result.Split('}');
                        for(int i = 0;i < vehicles.Length; i++)
                        {
                            vehicles[i] = vehicles[i].Replace("{", "").Replace("[", "").Replace("]","").Replace(","," ");
                            Console.WriteLine(vehicles[i]);
                        }
                        Console.ReadKey();
                    }
                    else
                    {
                        Console.WriteLine("Status:" + response.StatusCode);
                        Console.WriteLine("Result: ");
                        Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                        Console.ReadKey();
                    }
                }
            }
            catch (Exception e)
            {
                Console.Clear();
                Console.WriteLine(e.GetType());
                Console.WriteLine(e.Message);
                Console.ReadKey();
            }
        }

        public void OutTransactionHistory()
        {
            try
            {
                var url = PATH + "/transactions/all";
                using (var client = new HttpClient(BypassCertificate()))
                {
                    var response = Task.Run(async () => await client.GetAsync(url)).Result;
                    if(response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        string[] logs = response.Content.ReadAsStringAsync().Result.Split("\\n");
                        for(int i = 0;i < logs.Length; i++)
                        {
                            logs[i] = logs[i].Replace("\"","").Replace("\'"," ").Replace("\u0027","'");
                            Console.WriteLine(logs[i]);
                        }
                        Console.ReadKey();
                    } else
                    {
                        Console.WriteLine("Status:" + response.StatusCode);
                        Console.WriteLine("Result: ");
                        Console.ReadKey();
                    }
                    
                }
            }
            catch (Exception e)
            {
                Console.Clear();
                Console.WriteLine(e.GetType());
                Console.WriteLine(e.Message);
                Console.ReadKey();
            }
        }

        public void OutTransactionBeforeLog()
        {
            try
            {
                var url = PATH + "/transactions/last";
                using (var client = new HttpClient(BypassCertificate()))
                {
                    var response = Task.Run(async () => await client.GetAsync(url)).Result;
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        string[] transactions = response.Content.ReadAsStringAsync().Result.Replace("[", "").Replace("]", "").Split('}');
                        for(int i = 0;i < transactions.Length; i++)
                        {
                            transactions[i] = transactions[i].Replace("{","").Replace(",","");
                            Console.WriteLine(transactions[i]);
                        }
                    }
                    Console.ReadKey();
                }
            }
            catch (Exception e)
            {
                Console.Clear();
                Console.WriteLine(e.GetType());
                Console.WriteLine(e.Message);
                Console.ReadKey();
            }
        }

        public void OutIncomeBeforeLog()
        {
            try
            {
                var url = PATH + "/transactions/profit";
                using (var client = new HttpClient(BypassCertificate()))
                {
                    var response = Task.Run(async () => await client.GetAsync(url)).Result;
                    //Console.WriteLine("Status:" + response.StatusCode);
                    //Console.WriteLine("Result: ");
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        Console.WriteLine("Income for this period: " + response.Content.ReadAsStringAsync().Result);
                    }
                    else
                    {
                        Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                    }
                    Console.ReadKey();
                }
            }
            catch (Exception e)
            {
                Console.Clear();
                Console.WriteLine(e.GetType());
                Console.WriteLine(e.Message);
                Console.ReadKey();
            }
        }


        public void OutBalanceAtTheMoment()
        {
            try
            {
                var url = PATH + "/parking/balance";
                using (var client = new HttpClient(BypassCertificate()))
                {
                    var response = Task.Run(async () => await client.GetAsync(url)).Result;
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        Console.WriteLine("Park`s balance at the moment: " + response.Content.ReadAsStringAsync().Result);
                    }
                    else
                    {
                        Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                    }
                    Console.ReadKey();
                }
            }
            catch (Exception e)
            {
                Console.Clear();
                Console.WriteLine(e.GetType());
                Console.WriteLine(e.Message);
                Console.ReadKey();
            }
        }


        //********** Part for bypassing the certificate :)
        public HttpClientHandler BypassCertificate()
        {
            
            HttpClientHandler clientHandler = new HttpClientHandler();
            clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };
            return clientHandler;
            
        }
        //***************************************************
    }
}
