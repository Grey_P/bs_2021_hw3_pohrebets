﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoolParking.ConsoleClient.Services
{
    public class MenuService
    {
        private ClientService client;
        private bool exit = true;
        public MenuService(string path)
        {
            client = new ClientService(path);
        }

        public void start()
        {
            while (exit)
            {
                Menuintroduction();
            }
        }

        private void Menuintroduction()
        {
            Console.WriteLine("-----< Hello! >-----");
            Console.WriteLine("Please, chose action:");
            Console.WriteLine("1.Поставити транспортний засiб на парковку.");
            Console.WriteLine("2.Забрати транспортний засiб.");
            Console.WriteLine("3.Поповнити баланс конкретного Тр. засобу.");
            Console.WriteLine("4.Вивести на екран кiлькiсть вiльних мiсць на паркуваннi.");
            Console.WriteLine("5.Вивести на екран список Тр. засобiв , що знаходяться на Паркiнгу.");
            Console.WriteLine("6.Вивести на екран iсторiю Транзакцiй.");
            Console.WriteLine("7.Вивести на екран усi Транзакцiї Паркiнгу за поточний перiод (до запису у лог).");
            Console.WriteLine("8.Вивести на екран суму зароблених коштiв за поточний перiод (до запису у лог).");
            Console.WriteLine("9.Вивести на екран поточний баланс паркiнгу.");
            Console.WriteLine("0.Завершити сеанс.");
            Console.Write("Chose number--->  ");
            var val = Console.ReadLine();
            try
            {
                int a = int.Parse(val);
                if (a >= 0 && a <= 9)
                {
                    Console.Clear();
                    switch (a)
                    {
                        case 1:
                            client.AddTransportToParkingAsync();
                            break;
                        case 2:
                            client.RemoveTransportFromParking();
                            break;
                        case 3:
                            client.UppMoneyToTransport();
                            break;
                        case 4:
                            client.OutFreePlaces();
                            break;
                        case 5:
                            client.OutVehiclesOnParking();
                            break;
                        case 6:
                            client.OutTransactionHistory();
                            break;
                        case 7:
                            client.OutTransactionBeforeLog();
                            break;
                        case 8:
                            client.OutIncomeBeforeLog();
                            break;
                        case 9:
                            client.OutBalanceAtTheMoment();
                            break;
                        case 0:
                            Stop();
                            break;

                    }
                    Console.Clear();

                }
                else
                {
                    Console.Clear();
                    Console.WriteLine("Next time enter number from 1 to 9 pleeeease :)");
                    Console.ReadKey();
                    Console.Clear();
                }
            }
            catch
            {
                Console.Clear();
                Console.WriteLine("Incorrect input :( ");
                Console.ReadKey();
                Console.Clear();
                return;
            }
        }

        private void Stop()
        {
            exit = false;
            Console.WriteLine("Good luck)))");
            Console.ReadKey();
        }
    }
}
