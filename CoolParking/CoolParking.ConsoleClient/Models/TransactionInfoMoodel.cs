﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CoolParking.ConsoleClient.Models
{
    class TransactionInfoMoodel
    {
        [JsonProperty("vehicleId")]
        public string VehicleId { get; set; }

        [JsonProperty("sum")]
        public decimal Sum { get; set; }

        [JsonProperty("transactionDate")]
        public DateTime TransactionDate { get; set; }
    }
}
