﻿using CoolParking.BL.Interfaces;
using System.IO;
using System.Reflection;
using CoolParking.BL.Models;
// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        private string logpath;
        public string LogPath
        {
            get
            {
                return logpath;
            }
            private set
            {
                logpath = value;
            }
        }
        public LogService()
        {
            this.LogPath = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log";
        }
        public LogService(string path)
        {
            this.LogPath = path;
        }

        public string Read()
        {
            try
            {
                string actual;
                using (var file = new StreamReader(this.logpath))
                {
                    actual = file.ReadToEnd();
                }
                return actual;
            } catch
            {
                throw new System.InvalidOperationException();
            }
        }

        public void Write(string logInfo)
        {
            using (var file = new StreamWriter(this.logpath, true))
            {
                file.WriteLine(logInfo);
            }
        }
    }

}