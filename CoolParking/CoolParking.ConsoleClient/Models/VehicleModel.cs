﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CoolParking.ConsoleClient.Models
{
    class VehicleModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("vehicleType")]
        public int VehicleType { get; set; }

        [JsonProperty("balance")]
        public decimal Balance { get; set; }
    }
}
