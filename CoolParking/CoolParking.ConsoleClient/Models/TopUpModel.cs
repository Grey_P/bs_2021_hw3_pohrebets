﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CoolParking.ConsoleClient.Models
{
    class TopUpModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("Sum")]
        public decimal Sum { get; set; }
    }
}
