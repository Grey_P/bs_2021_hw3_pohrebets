﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Models
{
    public class TransactionInfoMoodel
    {
        [JsonProperty("vehicleId")]
        public string vehicleId { get; set; }

        [JsonProperty("sum")]
        public decimal sum { get; set; }

        [JsonProperty("transactionDate")]
        public DateTime transactionDate { get; set; }
    }
}
