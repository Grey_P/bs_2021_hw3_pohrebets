﻿using System;
using CoolParking.ConsoleClient.Services;

namespace CoolParking.ConsoleClient
{
    class Program
    { 
        static void Main(string[] args)
        {
            MenuService menu = new MenuService("https://localhost:5001/api");
            menu.start();
        }
    }
}
